﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PurchaseGuide : MonoBehaviour
{
    //A list of all posssible purchases
    private Dictionary<string, PossiblePurchase> possiblePurchases = new Dictionary<string, PossiblePurchase>();

    //The maximum budget available
    public float maxBudget = 0;

    //List of possible purhcase combinations
    private List<SolutionSet> possibleOutcomes = new List<SolutionSet>();

    //Settings for filtering/sorting the outcomes
    public bool filterSubLists = true;
    public int minNumItems = 2;
    public float minTotalCost = 0.5f;
    public float preferredTotalCost = 1.0f;

    //List of all the items the user has in their wanted list
    

    //References
    private InputField maxBudgetInput;
    private Transform itemScrollParent;
    private Transform outcomeScrollParent;
    private Text totalCostText;
    private GameObject nextButton;
    private GameObject previousButton;

	// Use this for initialization
	void Start ()
    {
        maxBudgetInput = GameObject.Find("MaxBudget").GetComponent<InputField>();
        maxBudgetInput.text = maxBudget.ToString();
        itemScrollParent = GameObject.Find("Items/Viewport/Content").transform;
        outcomeScrollParent = GameObject.Find("Outcomes/Possible Outcomes/Viewport/Content").transform;
        totalCostText = GameObject.Find("Outcomes/Outcome Cost").GetComponent<Text>();
        nextButton = GameObject.Find("Next");
        previousButton = GameObject.Find("Previous");
    }
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    //Add a new item to the list
    public void Add()
    {
        //Add a new item to the wanted list
        
    }
    //Begin calculating the possible purchases the user can make
    public void Calculate()
    {
        //We're gonna need a coroutine for this
        StartCoroutine(FindCombos());
    }

    private IEnumerator FindCombos()
    {
        //Sort all the purchases from cheapest to most expensive
        List<string> sortedPurchases = possiblePurchases.Keys.ToList();
        MergeSort(sortedPurchases);
        for (int i = 0; i < sortedPurchases.Count;)
        {
            Debug.Log(possiblePurchases[sortedPurchases[i]].price);
        }
        //Start by finding all of the possible combinations of items the user can buy
        for (int i = 0; i < sortedPurchases.Count; i++)
        {
            //Each item can only be added once (no two items have the same name)
            //The order of the items doesn't matter, but no two sets should have the same items in a different order
            //Use the reverse human approach, adding the smallest items first and working upwards
            //This approach works better because the solutions with more items are found first, so the solutions with less items can be ignored if another solution already contains them
        }
        //Filter out solutions the user does not want
        for (int i = possibleOutcomes.Count-1; i >= 0; i--)
        {
            //User can ignore solutions that don't have the minimum amount of items
            if (possibleOutcomes[i].items.Count < minNumItems)
                possibleOutcomes.RemoveAt(i);
            //User can also ignore solutions that are not close enough to the maximum budget
            if (possibleOutcomes[i].weight < maxBudget * minTotalCost)
                possibleOutcomes.RemoveAt(i);
            yield return new WaitForEndOfFrame();
        }
        //Sort/rank the remaining solutions based on what's most important to the user
            //User can prioritize having more items, more expensive items (larger price/weight), the priority level of the items, or some combination of the three
            //User can also choose to prioritize the solutions close to a certain price point, such as 75% of the total budget
        //Spawn the "best" solution
        yield return null;
    }
    //Taken from here: https://gist.github.com/pmgeorg/9122984
    private void MergeSort(List<string> input)
    {
        MergeSort(input, 0, input.Count - 1);
    }
    private void MergeSort(List<string> input, int low, int high)
    {
        if (low < high)
        {
            int middle = (low / 2) + (high / 2);
            MergeSort(input, low, middle);
            MergeSort(input, middle + 1, high);
            Merge(input, low, middle, high);
        }
    }

    private void Merge(List<string> input, int low, int middle, int high)
    {

        int left = low;
        int right = middle + 1;
        string[] tmp = new string[(high - low) + 1];
        int tmpIndex = 0;

        while ((left <= middle) && (right <= high))
        {
            if (possiblePurchases[input[left]].price < possiblePurchases[input[right]].price)
            {
                tmp[tmpIndex] = input[left];
                left = left + 1;
            }
            else
            {
                tmp[tmpIndex] = input[right];
                right = right + 1;
            }
            tmpIndex = tmpIndex + 1;
        }

        if (left <= middle)
        {
            while (left <= middle)
            {
                tmp[tmpIndex] = input[left];
                left = left + 1;
                tmpIndex = tmpIndex + 1;
            }
        }

        if (right <= high)
        {
            while (right <= high)
            {
                tmp[tmpIndex] = input[right];
                right = right + 1;
                tmpIndex = tmpIndex + 1;
            }
        }

        for (int i = 0; i < tmp.Length; i++)
        {
            input[low + i] = tmp[i];
        }

    }
}
