﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SolutionSet
{
    //The items in this set (can be in any order)
    public List<string> items = new List<string>();
    //The total cost (weight) of this set
    public float weight = 0;

    //Function that checks to see if a different solution set is contained within this one
    public bool Contains(List<string> other)
    {
        //It isn't necessary to know which entries aren't in this set, only that this set doesn't have all of them
        int entriesNotFound = other.Count;
        //Only go through this list once (even though Contains may iterate through the other set)
        for(int i = 0; i < items.Count; i++)
        {
            //Break if all the items have already been found
            if (entriesNotFound == 0)
                break;
            //Check if the other set has this item
            if (other.Contains(items[i]))
                entriesNotFound--;
        }
        //If all of the entries were found, the other set is present in this one
        if (entriesNotFound == 0)
            return true;
        else
            return false;
    }
}
